package com.cgi.management.data.repository;

import com.cgi.management.data.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {

@Query("SELECT r FROM Role r JOIN FETCH r.groups a WHERE r.groups = :idGroup")
    List<Role> findRoleByIdPerson(@Param("idGroup") Long idGroup);

}
