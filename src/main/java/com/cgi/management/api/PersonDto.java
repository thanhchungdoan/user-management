package com.cgi.management.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PersonDto {

    private Long idPerson;
    private String name;
    private String email;
    @JsonIgnore
    private String password;
    @JsonIgnore
    private int salary;
    @JsonIgnore
    private long idAddress;

    public PersonDto() {
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public long getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(long idAddress) {
        this.idAddress = idAddress;
    }

    @Override
    public String toString() {
        return "PersonDto{" +
                "idPerson=" + idPerson +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salary=" + salary +
                '}';
    }
}
