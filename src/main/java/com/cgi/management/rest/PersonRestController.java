package com.cgi.management.rest;

import com.cgi.management.api.PersonDto;
import com.cgi.management.service.impl.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/persons")
public class PersonRestController {

    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }

//  http://localhost:8082/management/api/v1/persons/id/1
    @GetMapping(path = "/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDto> getPersonById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(personService.findPersonById(id));
    }

    //  http://localhost:8082/management/api/v1/persons/email/david@gmail.com
    @GetMapping(path = "/email/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDto> getPersonByEmail(@PathVariable("email") String email) {
        return ResponseEntity.ok(personService.findPersonByEmail(email));
    }

}
