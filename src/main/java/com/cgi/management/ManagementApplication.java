package com.cgi.management;

import com.cgi.management.api.PersonDto;
import com.cgi.management.data.entities.Person;
import com.cgi.management.service.impl.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Optional;

@SpringBootApplication
public class ManagementApplication {

    public static void main(String[] args) {
//        ApplicationContext applicationContext =
                SpringApplication.run(ManagementApplication.class, args);
//        PersonService personService = applicationContext.getBean(PersonService.class);
//
//        PersonDto person = personService.findPersonById(2L);
//        System.out.println(person);
    }
}
