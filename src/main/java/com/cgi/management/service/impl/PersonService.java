package com.cgi.management.service.impl;


import com.cgi.management.api.PersonDto;
import com.cgi.management.data.entities.Person;
import com.cgi.management.data.repository.PersonRepository;
import com.cgi.management.service.mapping.BeanMapping;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;

@Service
@Transactional
public class PersonService {

    private PersonRepository personRepository;
    private BeanMapping beanMapping;

    @Autowired
    public PersonService(PersonRepository personRepository,
                         BeanMapping beanMapping) {
        this.personRepository = personRepository;
        this.beanMapping = beanMapping;
    }

    public PersonDto findPersonById(Long id) {
        Person person = personRepository.findById(id).orElseThrow(()
                -> new EntityNotFoundException("Entity with id: " + id + " not found."));
        return beanMapping.mapTo(person, PersonDto.class);
    }

    public PersonDto findPersonByEmail(String emai) {
        Person person = personRepository.findByEmail(emai).orElseThrow(()
                -> new EntityNotFoundException("Entity with id: " + emai + " not found."));
        return beanMapping.mapTo(person, PersonDto.class);
    }

}
